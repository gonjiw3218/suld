-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 11, 2021 at 09:00 AM
-- Server version: 5.7.32-cll-lve
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nanimnn_suld`
--

-- --------------------------------------------------------

--
-- Table structure for table `ewent`
--

CREATE TABLE `ewent` (
  `id` int(11) NOT NULL,
  `body_mon` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title_mon` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `time_s` date NOT NULL,
  `body_en` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `photo` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user` int(11) NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ewent`
--

INSERT INTO `ewent` (`id`, `body_mon`, `title_mon`, `time_s`, `body_en`, `title_en`, `photo`, `user`, `end_time`) VALUES
(2, '&lt;p&gt;Ñ‹Ð±Ð¹&lt;/p&gt;\r\n', 'Ð£Ð´Ð¸Ñ€Ð´Ð°Ñ… Ð·Ó©Ð²Ð»Ó©Ð»Ð¸Ð¹Ð½ Ñ…ÑƒÑ€Ð°Ð» 2019 Ð¾Ð½Ñ‹ 11-Ñ€ ÑÐ°Ñ€Ñ‹Ð½ 23, 30-Ð½Ð´ Ð±Ð¾Ð»Ð¶ Ð´Ð°Ñ€Ð°Ð°Ñ… Ð°ÑÑƒÑƒÐ´Ð»ÑƒÑƒÐ´Ñ‹Ð³ Ñ…ÑÐ»ÑÐ»Ñ†ÑÐ¶ ÑˆÐ¸Ð¹Ð´Ð²ÑÑ€Ð»ÑÐ»ÑÑ.', '2021-01-11', '&lt;p&gt;sadsa&lt;/p&gt;\r\n', 'Some analytical and statistical aspects of a stochastic volatility model of Ornstein-Uhlenbeck type', 'https://ichef.bbci.co.uk/news/976/cpsprodpb/421A/production/_112922961_apple.jpg', 89893218, '2021-01-12 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `img` varchar(150) NOT NULL,
  `url` varchar(150) NOT NULL,
  `body` varchar(150) NOT NULL,
  `add_row` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `img`, `url`, `body`, `add_row`) VALUES
(1, 'NEXUS NORTH AMERICA', 'https://www.nexusautomotiveinternational.eu/logos/members/nexusnorthamerica.jpg', 'https://vipar.com/news/the-network-apa-group-and-vipar-heavy-duty-announce-formation-of-nexus-north-america', 'United States of America', '');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `mon_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `en_name`, `mon_name`, `url`) VALUES
(1, 'Home', 'ÐÒ¯Ò¯Ñ€', 'index.php'),
(5, 'Company', 'ÐšÐ¾Ð¼Ð¿Ð°Ð½Ð¸ÑƒÐ´', 'company.php'),
(6, 'Innovation', 'Ð˜Ð½Ð½Ð¾Ð²Ð°Ñ†Ð¸', 'academy.php'),
(7, 'Information', 'ÐœÑÐ´ÑÑÐ»ÑÐ»', 'news.php'),
(8, 'Entrepreneurship', 'Ð‘Ð¸Ð·Ð½ÐµÑ ÑÑ€Ñ…Ð»ÑÐ»Ñ‚', 'ent');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `body_mon` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title_mon` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `time_s` date NOT NULL,
  `body_en` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `photo` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `body_mon`, `title_mon`, `time_s`, `body_en`, `title_en`, `photo`, `user`) VALUES
(5, '&lt;p&gt;mon test titlefsdfdsfdsf&lt;/p&gt;\r\n', 'mon test title', '2021-01-11', '&lt;p&gt;en test titlesfdsfdsf&lt;/p&gt;\r\n', 'en test title', 'https://ichef.bbci.co.uk/news/976/cpsprodpb/421A/production/_112922961_apple.jpg', 89893218);

-- --------------------------------------------------------

--
-- Table structure for table `nexus`
--

CREATE TABLE `nexus` (
  `id` int(11) NOT NULL,
  `mon_name` text NOT NULL,
  `en_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nexus`
--

INSERT INTO `nexus` (`id`, `mon_name`, `en_name`) VALUES
(1, '<p>NEXUSAUTO</p>\r\n', '<p>NEXUSAUTO</p>\r\n'),
(2, '<p>NEXUSTRUCK</p>\r\n', '<p>NEXUSTRUCK</p>\r\n'),
(3, '<p>SULD ACADEMY</p>\r\n', '<p>SULD ACADEMY</p>\r\n'),
(4, '<p>SHOPPY AUTOMOTIVE LLC</p>\r\n', '<p>SHOPPY AUTOMOTIVE LLC</p>\r\n'),
(5, '<p>SULD TRUCK LLC</p>\r\n', '<p>SULD TRUCK LLC</p>\r\n'),
(6, '<p>SULD LOGISTICS LLC</p>\r\n', '<p>SULD LOGISTICS LLC</p>\r\n'),
(7, '<p>GEO EXPERT LLC</p>\r\n', '<p>GEO EXPERT LLC</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `sub_menu`
--

CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL,
  `key_id` int(11) NOT NULL,
  `en_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mon_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `url` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_menu`
--

INSERT INTO `sub_menu` (`id`, `key_id`, `en_name`, `mon_name`, `url`) VALUES
(1, 5, 'SHOPPY AUTOMOTIVE LLC', 'SHOPPY AUTOMOTIVE LLC', 'shoppy_automotive.php'),
(2, 5, 'SULD TRUCK LLC ', 'Ð¡Ò®Ð›Ð” Ð¢Ð ÐÐš Ð¥Ð¥Ðš', 'suld_truck.php'),
(3, 5, 'SULD LOGISTICS LLC', 'Ð¡Ò®Ð›Ð” Ð›ÐžÐ–Ð˜Ð¡Ð¢Ð˜Ðš Ð¥Ð¥Ðš', 'suld_logistics.php'),
(4, 6, 'NEXUSAUTO', 'NEXUSAUTO', 'auto.php'),
(5, 6, 'NEXUSTRUCK', 'NEXUSTRUCK', 'truck.php'),
(6, 6, 'SULD ACADEMY ', 'Ð¡Ò®Ð›Ð” ÐÐšÐÐ”Ð•ÐœÐ˜ ', 'academy.php'),
(7, 7, 'Ewents', 'Ð­Ð²ÑÐ½Ñ‚Ñ', 'ewents.php'),
(8, 7, 'News', 'ÐœÑÐ´ÑÑÐ½Ò¯Ò¯Ð´', 'news.php'),
(9, 8, 'Members', 'Ð“Ð¸ÑˆÒ¯Ò¯Ð´', 'members.php'),
(10, 8, 'Suppliers', 'ÐÐ¸Ð¹Ð»Ò¯Ò¯Ð»ÑÐ³Ñ‡Ð¸Ð´', 'suppliers.php'),
(11, 5, 'GEO EXPERT LLC', 'Ð“Ð•Ðž Ð­ÐšÐ¡ÐŸÐ•Ð Ð¢ Ð¥Ð¥Ðš', 'geo_expert.php');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `img` varchar(150) NOT NULL,
  `url` varchar(150) NOT NULL,
  `body` varchar(150) NOT NULL,
  `add_row` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `img`, `url`, `body`, `add_row`) VALUES
(1, '', 'https://www.nexusautomotiveinternational.eu/logos/suppliers/bosch.jpg', 'https://www.bosch.com/products-and-services/mobility/', '', 'Strategic suppliers'),
(4, '', 'https://www.nexusautomotiveinternational.eu/logos/suppliers/brembo.jpg', 'http://www.brembo.com/en/', '', 'Strategic suppliers'),
(5, '', 'https://www.nexusautomotiveinternational.eu/logos/suppliers/arnott.jpg', 'https://www.arnotteurope.com/', '', 'Preferred suppliers'),
(6, '', 'https://www.nexusautomotiveinternational.eu/logos/suppliers/algo.jpg', 'http://www.algo.it/', '', 'Listed suppliers');

-- --------------------------------------------------------

--
-- Table structure for table `users_1`
--

CREATE TABLE `users_1` (
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `fb_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `f_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `l_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `bio` text CHARACTER SET utf8 COLLATE utf8_bin,
  `r_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `email_active` int(11) DEFAULT NULL,
  `fb_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_1`
--

INSERT INTO `users_1` (`user_id`, `username`, `email`, `user_type`, `password`, `phone`, `fb_id`, `end_date`, `start_date`, `f_name`, `l_name`, `bio`, `r_email`, `email_active`, `fb_name`, `photo`) VALUES
('683214905', 'baku', 'baku@admin.com', 'user', '431ab62b3b32f8c12bf47b05db0581ce', 0, 'fb_null', '2021-01-11', '2021-01-11', '', '', '', '', 0, '', 'img_null'),
('89893218', 'TOGTOKHOO', 'gonjiw3218@gmail.com', 'god', '32f716350b79bed56956c119e235df79', 89893218, '734715467428373', '2020-12-02', '2020-11-10', 'ÐÐ»Ñ‚Ð°Ð½-Ð¾Ñ‡Ð¸Ñ€', 'Ð‘ÑƒÑÐ½Ñ‚Ð¾Ð³Ñ‚Ð¾Ñ…', 'wwe', 'webmaster', 1, 'Ð. Ð¢Ð¾Ð³Ñ‚Ð¾Ñ…Ð¾Ð¾', 'https://mathsoc.mn/v2/uploads/webmaster.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_data`
--

CREATE TABLE `web_data` (
  `id` int(11) NOT NULL,
  `bc_color` varchar(500) NOT NULL,
  `bc_image` varchar(500) NOT NULL,
  `bc_type` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_data`
--

INSERT INTO `web_data` (`id`, `bc_color`, `bc_image`, `bc_type`) VALUES
(1, '#eef0f8', 'https://cdn.cjr.org/wp-content/uploads/2019/07/AdobeStock_100000042-e1563305717660-686x371.jpeg', '');

-- --------------------------------------------------------

--
-- Table structure for table `web_file`
--

CREATE TABLE `web_file` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `up_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `web_photo`
--

CREATE TABLE `web_photo` (
  `id` int(11) NOT NULL,
  `photo_name` varchar(255) DEFAULT NULL,
  `img_size` varchar(11) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `up_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_photo`
--

INSERT INTO `web_photo` (`id`, `photo_name`, `img_size`, `user`, `up_date`) VALUES
(3, 'thump_1609742086.png', '1024 x 0768', '89893218', '2021-01-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ewent`
--
ALTER TABLE `ewent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nexus`
--
ALTER TABLE `nexus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_1`
--
ALTER TABLE `users_1`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `web_data`
--
ALTER TABLE `web_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_file`
--
ALTER TABLE `web_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_photo`
--
ALTER TABLE `web_photo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ewent`
--
ALTER TABLE `ewent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `nexus`
--
ALTER TABLE `nexus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sub_menu`
--
ALTER TABLE `sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `web_data`
--
ALTER TABLE `web_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_file`
--
ALTER TABLE `web_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `web_photo`
--
ALTER TABLE `web_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
